package com.openbanking.openbankingapi.services;

import com.openbanking.openbankingapi.domain.Transaction;
import com.openbanking.openbankingapi.domain.TransactionType;
import com.openbanking.openbankingapi.repository.TransactionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Date;
import java.util.Set;

@SpringBootTest
@AutoConfigureMockMvc
public class TransactionServiceTest {

    @Autowired
    private TransactionService transactionService;

    @MockBean
    private TransactionRepository transactionRepository;


    @Test
    public void testFindAllByAccountNumber() {
        Set<Transaction> transactionSet = Set.of(
                new Transaction(2l, TransactionType.CREDIT, new Date(), "123", "EUR", 123.01, "NAME", "LOGO"),
                new Transaction(3l, TransactionType.DEBT, new Date(), "123", "EUR", 12.02, "NAME", "LOGO"),
                new Transaction(4l, TransactionType.DEBT, new Date(), "123", "EUR", 11.01, "NAME", "LOGO")
        );


        Mockito.when(transactionRepository.findByAccountNumber(Mockito.anyString()))
                .thenReturn(transactionSet);

        Set<Transaction> result = this.transactionService.findAllByAccountNumber("1234");
        Assertions.assertNotNull(result);
        int resultSize = result.size();
        Assertions.assertTrue(resultSize == 3, "findAllByAccountNumber should return between 3 transactions");
    }
}
