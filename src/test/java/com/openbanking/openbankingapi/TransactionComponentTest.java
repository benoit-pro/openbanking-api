package com.openbanking.openbankingapi;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.*;
import static org.hamcrest.Matchers.*;


import com.openbanking.openbankingapi.controller.TransactionController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

@SpringBootTest
public class TransactionComponentTest {
    @Autowired
    private TransactionController transactionController;
    @Test
    public void testGetTransactions() {
        given().standaloneSetup(transactionController).when().get("/transactions/123-33").then().status(HttpStatus.OK)
                .body("[0].accountNumber", equalTo("123-33"));
    }
}
