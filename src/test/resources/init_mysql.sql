CREATE TABLE `transaction` (
  `id` bigint NOT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `amount` decimal(10,2) NOT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `date` datetime(6) DEFAULT NULL,
  `merchant_logo` varchar(255) DEFAULT NULL,
  `merchant_name` varchar(255) DEFAULT NULL,
  `type` int DEFAULT NULL,
  PRIMARY KEY (`id`)
);


INSERT INTO transaction SET account_number='123-33', amount=123.12, currency='EUR', date=CURRENT_DATE, merchant_logo='LOGO', merchant_name='NAME', type=1, id=1;