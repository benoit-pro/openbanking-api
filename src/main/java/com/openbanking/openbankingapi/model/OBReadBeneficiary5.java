package com.openbanking.openbankingapi.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.openbanking.openbankingapi.model.Links;
import com.openbanking.openbankingapi.model.Meta;
import com.openbanking.openbankingapi.model.OBReadBeneficiary5Data;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * OBReadBeneficiary5
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-10-22T13:35:47.542611896-04:00[America/New_York]")
public class OBReadBeneficiary5 {

  private OBReadBeneficiary5Data data;

  private Links links;

  private Meta meta;

  /**
   * Default constructor
   * @deprecated Use {@link OBReadBeneficiary5#OBReadBeneficiary5(OBReadBeneficiary5Data)}
   */
  @Deprecated
  public OBReadBeneficiary5() {
    super();
  }

  /**
   * Constructor with only required parameters
   */
  public OBReadBeneficiary5(OBReadBeneficiary5Data data) {
    this.data = data;
  }

  public OBReadBeneficiary5 data(OBReadBeneficiary5Data data) {
    this.data = data;
    return this;
  }

  /**
   * Get data
   * @return data
  */
  @NotNull @Valid 
  @Schema(name = "Data", requiredMode = Schema.RequiredMode.REQUIRED)
  @JsonProperty("Data")
  public OBReadBeneficiary5Data getData() {
    return data;
  }

  public void setData(OBReadBeneficiary5Data data) {
    this.data = data;
  }

  public OBReadBeneficiary5 links(Links links) {
    this.links = links;
    return this;
  }

  /**
   * Get links
   * @return links
  */
  @Valid 
  @Schema(name = "Links", requiredMode = Schema.RequiredMode.NOT_REQUIRED)
  @JsonProperty("Links")
  public Links getLinks() {
    return links;
  }

  public void setLinks(Links links) {
    this.links = links;
  }

  public OBReadBeneficiary5 meta(Meta meta) {
    this.meta = meta;
    return this;
  }

  /**
   * Get meta
   * @return meta
  */
  @Valid 
  @Schema(name = "Meta", requiredMode = Schema.RequiredMode.NOT_REQUIRED)
  @JsonProperty("Meta")
  public Meta getMeta() {
    return meta;
  }

  public void setMeta(Meta meta) {
    this.meta = meta;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OBReadBeneficiary5 obReadBeneficiary5 = (OBReadBeneficiary5) o;
    return Objects.equals(this.data, obReadBeneficiary5.data) &&
        Objects.equals(this.links, obReadBeneficiary5.links) &&
        Objects.equals(this.meta, obReadBeneficiary5.meta);
  }

  @Override
  public int hashCode() {
    return Objects.hash(data, links, meta);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OBReadBeneficiary5 {\n");
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("    links: ").append(toIndentedString(links)).append("\n");
    sb.append("    meta: ").append(toIndentedString(meta)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

