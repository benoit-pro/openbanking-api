package com.openbanking.openbankingapi.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Amount sub type, in a coded form.
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-10-22T13:35:47.542611896-04:00[America/New_York]")
public enum OBExternalBalanceSubType1Code {
  
  BASECURRENCY("BaseCurrency"),
  
  LOCALCURRENCY("LocalCurrency");

  private String value;

  OBExternalBalanceSubType1Code(String value) {
    this.value = value;
  }

  @JsonValue
  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static OBExternalBalanceSubType1Code fromValue(String value) {
    for (OBExternalBalanceSubType1Code b : OBExternalBalanceSubType1Code.values()) {
      if (b.value.equals(value)) {
        return b;
      }
    }
    throw new IllegalArgumentException("Unexpected value '" + value + "'");
  }
}

