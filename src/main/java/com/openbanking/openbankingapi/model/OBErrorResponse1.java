package com.openbanking.openbankingapi.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.openbanking.openbankingapi.model.OBError1;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * An array of detail error codes, and messages, and URLs to documentation to help remediation.
 */

@Schema(name = "OBErrorResponse1", description = "An array of detail error codes, and messages, and URLs to documentation to help remediation.")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-10-22T13:35:47.542611896-04:00[America/New_York]")
public class OBErrorResponse1 {

  private String code;

  private String id;

  private String message;

  @Valid
  private List<@Valid OBError1> errors = new ArrayList<>();

  /**
   * Default constructor
   * @deprecated Use {@link OBErrorResponse1#OBErrorResponse1(String, String, List<@Valid OBError1>)}
   */
  @Deprecated
  public OBErrorResponse1() {
    super();
  }

  /**
   * Constructor with only required parameters
   */
  public OBErrorResponse1(String code, String message, List<@Valid OBError1> errors) {
    this.code = code;
    this.message = message;
    this.errors = errors;
  }

  public OBErrorResponse1 code(String code) {
    this.code = code;
    return this;
  }

  /**
   * High level textual error code, to help categorize the errors.
   * @return code
  */
  @NotNull @Size(min = 1, max = 40) 
  @Schema(name = "Code", description = "High level textual error code, to help categorize the errors.", requiredMode = Schema.RequiredMode.REQUIRED)
  @JsonProperty("Code")
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public OBErrorResponse1 id(String id) {
    this.id = id;
    return this;
  }

  /**
   * A unique reference for the error instance, for audit purposes, in case of unknown/unclassified errors.
   * @return id
  */
  @Size(min = 1, max = 40) 
  @Schema(name = "Id", description = "A unique reference for the error instance, for audit purposes, in case of unknown/unclassified errors.", requiredMode = Schema.RequiredMode.NOT_REQUIRED)
  @JsonProperty("Id")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public OBErrorResponse1 message(String message) {
    this.message = message;
    return this;
  }

  /**
   * Brief Error message, e.g., 'There is something wrong with the request parameters provided'
   * @return message
  */
  @NotNull @Size(min = 1, max = 500) 
  @Schema(name = "Message", description = "Brief Error message, e.g., 'There is something wrong with the request parameters provided'", requiredMode = Schema.RequiredMode.REQUIRED)
  @JsonProperty("Message")
  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public OBErrorResponse1 errors(List<@Valid OBError1> errors) {
    this.errors = errors;
    return this;
  }

  public OBErrorResponse1 addErrorsItem(OBError1 errorsItem) {
    if (this.errors == null) {
      this.errors = new ArrayList<>();
    }
    this.errors.add(errorsItem);
    return this;
  }

  /**
   * Get errors
   * @return errors
  */
  @NotNull @Valid @Size(min = 1) 
  @Schema(name = "Errors", requiredMode = Schema.RequiredMode.REQUIRED)
  @JsonProperty("Errors")
  public List<@Valid OBError1> getErrors() {
    return errors;
  }

  public void setErrors(List<@Valid OBError1> errors) {
    this.errors = errors;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OBErrorResponse1 obErrorResponse1 = (OBErrorResponse1) o;
    return Objects.equals(this.code, obErrorResponse1.code) &&
        Objects.equals(this.id, obErrorResponse1.id) &&
        Objects.equals(this.message, obErrorResponse1.message) &&
        Objects.equals(this.errors, obErrorResponse1.errors);
  }

  @Override
  public int hashCode() {
    return Objects.hash(code, id, message, errors);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OBErrorResponse1 {\n");
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("    errors: ").append(toIndentedString(errors)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

