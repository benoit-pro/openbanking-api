package com.openbanking.openbankingapi.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.openbanking.openbankingapi.model.Links;
import com.openbanking.openbankingapi.model.Meta;
import com.openbanking.openbankingapi.model.OBReadDirectDebit2Data;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * OBReadDirectDebit2
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-10-22T13:35:47.542611896-04:00[America/New_York]")
public class OBReadDirectDebit2 {

  private OBReadDirectDebit2Data data;

  private Links links;

  private Meta meta;

  /**
   * Default constructor
   * @deprecated Use {@link OBReadDirectDebit2#OBReadDirectDebit2(OBReadDirectDebit2Data)}
   */
  @Deprecated
  public OBReadDirectDebit2() {
    super();
  }

  /**
   * Constructor with only required parameters
   */
  public OBReadDirectDebit2(OBReadDirectDebit2Data data) {
    this.data = data;
  }

  public OBReadDirectDebit2 data(OBReadDirectDebit2Data data) {
    this.data = data;
    return this;
  }

  /**
   * Get data
   * @return data
  */
  @NotNull @Valid 
  @Schema(name = "Data", requiredMode = Schema.RequiredMode.REQUIRED)
  @JsonProperty("Data")
  public OBReadDirectDebit2Data getData() {
    return data;
  }

  public void setData(OBReadDirectDebit2Data data) {
    this.data = data;
  }

  public OBReadDirectDebit2 links(Links links) {
    this.links = links;
    return this;
  }

  /**
   * Get links
   * @return links
  */
  @Valid 
  @Schema(name = "Links", requiredMode = Schema.RequiredMode.NOT_REQUIRED)
  @JsonProperty("Links")
  public Links getLinks() {
    return links;
  }

  public void setLinks(Links links) {
    this.links = links;
  }

  public OBReadDirectDebit2 meta(Meta meta) {
    this.meta = meta;
    return this;
  }

  /**
   * Get meta
   * @return meta
  */
  @Valid 
  @Schema(name = "Meta", requiredMode = Schema.RequiredMode.NOT_REQUIRED)
  @JsonProperty("Meta")
  public Meta getMeta() {
    return meta;
  }

  public void setMeta(Meta meta) {
    this.meta = meta;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OBReadDirectDebit2 obReadDirectDebit2 = (OBReadDirectDebit2) o;
    return Objects.equals(this.data, obReadDirectDebit2.data) &&
        Objects.equals(this.links, obReadDirectDebit2.links) &&
        Objects.equals(this.meta, obReadDirectDebit2.meta);
  }

  @Override
  public int hashCode() {
    return Objects.hash(data, links, meta);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OBReadDirectDebit2 {\n");
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("    links: ").append(toIndentedString(links)).append("\n");
    sb.append("    meta: ").append(toIndentedString(meta)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

