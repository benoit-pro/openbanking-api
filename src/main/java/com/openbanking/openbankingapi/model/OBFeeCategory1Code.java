package com.openbanking.openbankingapi.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Categorisation of fees and charges into standard categories.
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-10-22T13:35:47.542611896-04:00[America/New_York]")
public enum OBFeeCategory1Code {
  
  FCOT("FCOT"),
  
  FCRE("FCRE"),
  
  FCSV("FCSV");

  private String value;

  OBFeeCategory1Code(String value) {
    this.value = value;
  }

  @JsonValue
  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static OBFeeCategory1Code fromValue(String value) {
    for (OBFeeCategory1Code b : OBFeeCategory1Code.values()) {
      if (b.value.equals(value)) {
        return b;
      }
    }
    throw new IllegalArgumentException("Unexpected value '" + value + "'");
  }
}

