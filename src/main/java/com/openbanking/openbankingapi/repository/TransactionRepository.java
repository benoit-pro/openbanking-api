package com.openbanking.openbankingapi.repository;

import com.openbanking.openbankingapi.domain.Transaction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface TransactionRepository extends CrudRepository<Transaction, Long> {
    Set<Transaction> findByAccountNumber(String accountNumber);
}
