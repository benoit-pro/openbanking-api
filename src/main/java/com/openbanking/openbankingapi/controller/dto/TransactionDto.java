package com.openbanking.openbankingapi.controller.dto;

import com.openbanking.openbankingapi.domain.TransactionType;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Builder
@Data
public class TransactionDto {
    String type;
    Date date;
    String accountNumber;
    String currency;
    double amount;
    String merchantName;
    String merchantLogo;
}
