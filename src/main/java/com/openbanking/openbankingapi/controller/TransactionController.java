package com.openbanking.openbankingapi.controller;

import com.openbanking.openbankingapi.controller.dto.TransactionDto;
import com.openbanking.openbankingapi.controller.mapper.TransactionMapper;
import com.openbanking.openbankingapi.domain.Transaction;
import com.openbanking.openbankingapi.services.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
public class TransactionController {

    @Autowired
    TransactionService transactionService;
    @GetMapping("/transactions/{id}")
    public ResponseEntity<List<TransactionDto>> getAllTransactions(@PathVariable("id") String id) {
        Set<Transaction> transactions = transactionService.findAllByAccountNumber(id);
        List<TransactionDto> transactionDtos = transactions.stream().map(TransactionMapper::mapTransactionToTransactionDto).collect(Collectors.toList());
        return new ResponseEntity<>(transactionDtos, HttpStatus.OK);
    }
}
