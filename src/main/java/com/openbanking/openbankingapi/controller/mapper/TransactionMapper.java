package com.openbanking.openbankingapi.controller.mapper;

import com.openbanking.openbankingapi.controller.dto.TransactionDto;
import com.openbanking.openbankingapi.domain.Transaction;

public class TransactionMapper {

    public static TransactionDto mapTransactionToTransactionDto(Transaction transaction) {
        return TransactionDto.builder()
                .type(transaction.getType().toString())
                .date(transaction.getDate())
                .accountNumber(transaction.getAccountNumber())
                .amount(transaction.getAmount().doubleValue())
                .merchantName(transaction.getMerchantName())
                .merchantLogo(transaction.getMerchantLogo())
                .build();
    }
}
