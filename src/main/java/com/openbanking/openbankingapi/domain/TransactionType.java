package com.openbanking.openbankingapi.domain;

public enum TransactionType {
    DEBT,
    CREDIT
}
