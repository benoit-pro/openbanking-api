package com.openbanking.openbankingapi.services;

import com.openbanking.openbankingapi.domain.Transaction;
import com.openbanking.openbankingapi.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Set;

@Service
public class TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    public Set<Transaction> findAllByAccountNumber(String accountNumber) {
        return transactionRepository.findByAccountNumber(accountNumber);
    }
}
